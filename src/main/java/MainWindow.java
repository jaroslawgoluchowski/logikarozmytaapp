import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.Time;
import java.util.ArrayList;

import static java.lang.Thread.*;

public class MainWindow extends JFrame {
    private JButton pobierzDaneButton;
    private JButton start;
    private JPanel Pane;
    private JButton Logika;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField txtTemp;
    private JTextField txtWilg;
    private JTextArea TextPane;
    private ArrayList<Temp> lista;
    private FuzzyLogicv2 fuzzyLogic = new FuzzyLogicv2();
    private ArrayList<FuzzyLogicv2> listaLogiki;
    private ArrayList<Temp> testList;
    static final int min_slid = 0;
    final int mx_slid = 19;
    static final int init_slid = 0;
    private boolean czy_logika = false;
    private JSlider slider1;


    public MainWindow() {
        add(Pane);
        setTitle("Logika Rozmyta");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize(500, 500);
        slider1.setMajorTickSpacing(1);
        slider1.setMinorTickSpacing(1);
        slider1.setPaintTicks(true);
        slider1.setPaintLabels(true);
        slider1.setMaximum(mx_slid);
        slider1.setMinimum(min_slid);
        slider1.setValue(init_slid);
        testList = generateTestTempList();


        pobierzDaneButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    Temp.downloadData();
                } catch (IOException e1) {
                    JOptionPane.showMessageDialog(Pane, "Błąd pobierania danych", "Downloading error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });


        start.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                testList = generateTestTempList();
                listaLogiki = new ArrayList<FuzzyLogicv2>();
                for ( Temp item : testList ){
                    listaLogiki.add(fuzzyLogic.wynikLogiki(item.getTemp(),item.getHumidity()));
                }
            }
        });
        slider1.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                    FuzzyLogicv2 item = listaLogiki.get(slider1.getValue());
                    textField1.setText("" + item.getWynikczasu());
                    textField2.setText("" + item.getWynikmocy());
                    Temp item2 = testList.get(slider1.getValue());
                    txtTemp.setText("" + item2.getTemp());
                    txtWilg.setText("" + item2.getHumidity());
            }
        });
    }

    private ArrayList<Temp> generateTestTempList() {
        ArrayList<Temp> testList = new ArrayList<Temp>();

        testList.add(new Temp(0,10));
        testList.add(new Temp(10,20));
        testList.add(new Temp(20,30));
        testList.add(new Temp(30,40));
        testList.add(new Temp(40,50));
        testList.add(new Temp(14,60));
        testList.add(new Temp(26,70));
        testList.add(new Temp(36,90));
        testList.add(new Temp(0,100));
        testList.add(new Temp(40,20));
        return testList;
    }


    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                MainWindow mainWindow = new MainWindow();

                mainWindow.setVisible(true);
            }
        });

    }
}



