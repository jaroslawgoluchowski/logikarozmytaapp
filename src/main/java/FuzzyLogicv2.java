import lombok.Getter;
import lombok.NoArgsConstructor;
@NoArgsConstructor
public class FuzzyLogicv2 {

    private double[] temp1 = new double[92];
    private double[] temp2 = new double[92];    //niska
    private double[] temp3 = new double[92];    //srednia
    private double[] temp4 = new double[92];    //wysoka
    private double[] wilg1 = new double[202];
    private double[] wilg2 = new double[202];    //za mała
    private  double[] wilg3 = new double[202];    //dobra
    private double[] wilg4 = new double[202];    //za duża

    private double[] pompa1 = new double[202];
    private double[] pompa2 = new double[202];       // 0 moc
    private double[] pompa3 = new double[202];       //min moc
    private double[] pompa4 = new double[202];       //avg moc
    private double[] pompa5 = new double[202];       //big moc
    private double[] czas1 = new double[122];
    private  double[] czas2 = new double[122];
    private double[] czas3 = new double[122];
    private double[] czas4 = new double[122];
    private  double[] czas5 = new double[122];
    private double licznikmocy;
    private double licznikczasu;
    private double mianownikmocy;
    private double mianownikczasu;
    @Getter
    private double wynikmocy;
    @Getter
    private double wynikczasu;

    private double tempniska;
    private double tempsrednia;
    private double tempwysoka;
    private double wilgniska;
    private double wilgdobra;
    private double wilgwysoka;
    private double[][] mocwyjscia = new double[4][4];
    private double[][] czaswyjsia = new double[4][4];
    private double[][] wyjsciemocykoncowe = new double[7][202];
    private double[][] wyjscieczasukoncowe = new double[7][122];

    private double wz1,wz2,wn1,wn2,ws1,ws2,ww1,ww2;

    FuzzyLogicv2( Double mocKoncowa, Double czasKoncowy ){
        this.wynikmocy = mocKoncowa;
        this.wynikczasu = czasKoncowy;
    }

    //termy dla temperatury
    public void rozmycie_temperatury() {
        ;
        for (int i = 1; i < 91; i++) {
            temp1[i] = (i - 1) / 2;
            double x = temp1[i];
            if (x >= 0 && x <= 10) {
                temp2[i] = 1;
            } else if (x > 10 && x < 15) {
                temp2[i] = (15 - x) / (15 - 10);
            } else temp2[i] = 0;


            if (x > 10 && x <= 15) {
                temp3[i] = (x - 10) / (15 - 10);
            } else if (x > 15 && x < 25) {
                temp3[i] = (25 - x) / (25 - 15);
            } else temp3[i] = 0;
            if (x > 20 && x <= 35) {
                temp4[i] = (x - 20) / (35 - 20);
            } else if (x > 35 && x < 45) {
                temp4[i] = 1;
            } else temp4[i] = 0;
        }

    }

    public void rozmycie_gleby() {
        for (int i = 1; i < 201; i++) {

            wilg1[i] = (i - 1) / 2;
            double x = wilg1[i];
            if (x >= 0 && x <= 50) {
                wilg2[i] = 1;
            } else if (x > 50 && x < 70) {
                wilg2[i] = (70 - x) / (70 - 50);
            } else wilg2[i] = 0;


            if (x > 60 && x <= 70) {
                wilg3[i] = (x - 60) / (70 - 60);
            } else if (x > 70 && x <= 80) {
                wilg3[i] = 1;
            } else if (x > 80 && x <= 90) {
                wilg3[i] = (90 - x) / (90 - 80);
            } else wilg3[i] = 0;


            if (x > 80 && x <= 95) {
                wilg4[i] = (x - 80) / (95 - 80);
            } else if (x > 95 && x < 100) {
                wilg4[i] = 1;
            } else wilg4[i] = 0;
        }
    }

    public void rozmycie_mocy() {

        for (int i = 1; i < 201; i++) {
            pompa1[i] = (i - 1) / 2;
            double x = pompa1[i];
            if (x >= 0 && x <= 5) {
                pompa2[i] = 1;
            } else if (x > 5 && x < 25) {
                pompa2[i] = (25 - x) / (25 - 5);
            } else pompa2[i] = 0;


            if (x >= 10 && x <= 35) {
                pompa3[i] = (x - 10) / (35 - 10);
            } else if (x > 35 && x <= 45) {
                pompa3[i] = (45 - x) / (45 - 35);
            } else pompa3[i] = 0;


            if (x > 35 && x <= 45) {
                pompa4[i] = (x - 35) / (45 - 35);
            } else if (x > 45 && x < 70) {
                pompa4[i] = (70 - x) / (70 - 45);
            } else pompa4[i] = 0;

            if (x > 45 && x <= 70) {
                pompa5[i] = (x - 45) / (70 - 45);
            } else if (x > 70 && x <=90) {
                pompa5[i] = 1;}
                else if (x > 90 && x <=100) {
                    pompa5[i] =(100 - x) / (100 - 90);
            } else pompa5[i] = 0;
        }



        }
    public void rozmycie_czasu() {

        for (int i = 1; i < 121; i++) {
            czas1[i] = (i - 1) / 2;
            double x = czas1[i];
            if (x >= 0 && x <= 5) {
                czas2[i] = 1;
            } else if (x > 5 && x < 15) {
                czas2[i] = (15 - x) / (15 - 5);
            } else czas2[i] = 0;


            if (x >= 10 && x <= 25) {
                czas3[i] = (x - 10) / (25 - 10);
            } else if (x > 25 && x <= 40) {
                czas3[i] = (40 - x) / (40 - 25);
            } else czas3[i] = 0;


            if (x > 25 && x <= 40) {
                czas4[i] = (x - 25) / (40 - 25);
            } else if (x > 30 && x < 55) {
                czas4[i] = (55 - x) / (55 - 30);
            } else czas4[i] = 0;

            if (x > 55 && x <= 90) {
                czas5[i] = (x - 90) / (90 - 55);
            } else if (x > 90 && x <=110) {
                czas5[i] = 1;}
            else if (x > 110 && x <=120) {
                czas5[i] =(120 - x) / (120 - 110);
            } else czas5[i] = 0;
        }



    }

    public void reguly_rozmyte(double tm , double wil) {

        tm=2*tm+1;
        tempniska=temp2[(int) tm];
        tempsrednia=temp3[(int) tm];
        tempwysoka=temp4[(int) tm];
        wil=2*wil+1;
        wilgniska=wilg2[(int)wil];
        wilgdobra=wilg3[(int)wil];
        wilgwysoka=wilg4[(int)wil];



    //dla mocy zerowej
        if (Math.min(tempniska,wilgdobra)>0)
        {
            mocwyjscia[1][2]=Math.min(tempniska,wilgdobra);
            czaswyjsia[1][2]=Math.min(tempniska,wilgdobra);

        }
        if (Math.min(tempniska,wilgwysoka)>0)
        {
            mocwyjscia[1][3]=Math.min(tempniska,wilgwysoka);
            czaswyjsia[1][3]=Math.min(tempniska,wilgwysoka);
        }
        if (Math.min(tempsrednia,wilgwysoka)>0)
        {
            mocwyjscia[2][3]=Math.min(tempsrednia,wilgwysoka);
            czaswyjsia[2][3]=Math.min(tempsrednia,wilgwysoka);
        }

//dla mocy niskiej

        if (Math.min(tempniska,wilgniska)>0)
        {
            mocwyjscia[1][1]=Math.min(tempniska,wilgniska);
            czaswyjsia[1][1]=Math.min(tempniska,wilgniska);
        }//
        if (Math.min(tempsrednia,wilgdobra)>0)
        {
            mocwyjscia[2][2]=Math.min(tempsrednia,wilgdobra);
            czaswyjsia[2][2]=Math.min(tempsrednia,wilgdobra);
        }//

        if (Math.min(tempwysoka,wilgwysoka)>0)
        {
            mocwyjscia[3][3]=Math.min(tempwysoka,wilgwysoka);
            czaswyjsia[3][3]=Math.min(tempwysoka,wilgwysoka);
        }//




        //moc umiarkowana

        if (Math.min(tempsrednia,wilgniska)>0)
        {
            mocwyjscia[2][1]=Math.min(tempsrednia,wilgniska);
            czaswyjsia[2][1]=Math.min(tempsrednia,wilgniska);
        }//

        if (Math.min(tempwysoka,wilgdobra)>0)
        {
            mocwyjscia[3][2]=Math.min(tempwysoka,wilgdobra);
            czaswyjsia[3][2]=Math.min(tempwysoka,wilgdobra);
        }//

        //moc wielka
        if (Math.min(tempwysoka,wilgniska)>0)
        {
            mocwyjscia[3][1]=Math.min(tempwysoka,wilgniska);
            czaswyjsia[3][1]=Math.min(tempwysoka,wilgniska);
        }//





    }

void wnioskowanie() {
//zerowe
    wz1 = Math.max(mocwyjscia[1][2], Math.max(mocwyjscia[1][3], mocwyjscia[2][3]));
    wz2 = Math.max(mocwyjscia[1][2], Math.max(mocwyjscia[1][3], mocwyjscia[2][3]));

    //niskie
    wn1 = Math.max(mocwyjscia[1][1], Math.max(mocwyjscia[2][2], mocwyjscia[3][3]));
    wn2 = Math.max(mocwyjscia[1][1], Math.max(mocwyjscia[2][2], mocwyjscia[3][3]));
    //srednie
    ws1 = Math.max(mocwyjscia[2][1], mocwyjscia[3][2]);
    ws2 = Math.max(mocwyjscia[2][1], mocwyjscia[3][2]);

    //wysokie
    ww1 = mocwyjscia[3][1];
    ww2 = czaswyjsia[3][1];


    for (int i = 1; i < 201; i++) {
        //wz
        wyjsciemocykoncowe[1][i] = pompa1[i];
        if (pompa2[i] > wz1) {
            wyjsciemocykoncowe[2][i] = wz1;

        } else {
            wyjsciemocykoncowe[2][i] = pompa2[i];
        }
//wn
        if (pompa3[i] > wn1) {
            wyjsciemocykoncowe[3][i] = wn1;

        } else {
            wyjsciemocykoncowe[3][i] = pompa3[i];
        }
//ws

        if (pompa4[i] > ws1) {
            wyjsciemocykoncowe[4][i] = ws1;

        } else {
            wyjsciemocykoncowe[4][i] = pompa4[i];
        }
        //ww


        if (pompa5[i] > ww1) {
            wyjsciemocykoncowe[5][i] = ws1;

        } else {
            wyjsciemocykoncowe[5][i] = pompa5[i];

        }


        wyjsciemocykoncowe[6][i]=Math.max(wyjsciemocykoncowe[2][i],Math.max(wyjsciemocykoncowe[3][i],Math.max(wyjsciemocykoncowe[4][i],wyjsciemocykoncowe[5][i])));
        licznikmocy=licznikmocy+wyjsciemocykoncowe[1][i]*wyjsciemocykoncowe[6][i];
        mianownikmocy=mianownikmocy+wyjsciemocykoncowe[6][i];

    }

    for (int i = 1; i < 121; i++) {
        //wz
        wyjscieczasukoncowe[1][i] = czas1[i];
        if (pompa2[i] > wz1) {
            wyjscieczasukoncowe[2][i] = wz2;

        } else {
            wyjscieczasukoncowe[2][i] = czas2[i];
        }
//wn
        if (pompa3[i] > wn1) {
            wyjscieczasukoncowe[3][i] = wn2;

        } else {
            wyjscieczasukoncowe[3][i] = czas3[i];
        }
//ws

        if (pompa4[i] > ws1) {
            wyjscieczasukoncowe[4][i] = ws2;

        } else {
            wyjscieczasukoncowe[4][i] = czas4[i];
        }
        //ww


        if (pompa5[i] > ww1) {
            wyjscieczasukoncowe[5][i] = ws2;

        } else {
            wyjscieczasukoncowe[5][i] = czas5[i];

        }


        wyjscieczasukoncowe[6][i]=Math.max( wyjscieczasukoncowe[2][i],Math.max( wyjscieczasukoncowe[3][i],Math.max( wyjscieczasukoncowe[4][i],wyjscieczasukoncowe[5][i])));
        licznikczasu=licznikczasu+wyjscieczasukoncowe[1][i]*wyjscieczasukoncowe[6][i];
        mianownikczasu=mianownikczasu+wyjscieczasukoncowe[6][i];

    }


    wynikczasu=licznikczasu/mianownikczasu;
    wynikmocy=licznikmocy/mianownikmocy;


}

public FuzzyLogicv2 wynikLogiki( double Temperatura, double WilgotnoscGleby){

        rozmycie_czasu();
        rozmycie_mocy();
        rozmycie_gleby();
        rozmycie_temperatury();
        reguly_rozmyte(Temperatura, WilgotnoscGleby);
        wnioskowanie();

        return new FuzzyLogicv2(wynikczasu, wynikmocy);

}


}
