import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class Temp {
    private double temp;
    //private double pressure;
    private double humidity;

    //static ArrayList listaWilgotnosci = new ArrayList<Double>();
    //static ArrayList listaTemperatur = new ArrayList<Double>();

    public static void downloadData() throws IOException {

        URL url = new URL("http://api.openweathermap.org/data/2.5/forecast?id=3095049&units=metric&APPID=8a0df3aece19b3da2c9e49291b9944a6");
        URLConnection urlConnection = url.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        String inputLine;
        FileWriter fw = new FileWriter("dane.txt");

        if ((inputLine = in.readLine()) != null){
            fw.write(inputLine);
        }
        fw.close();
        in.close();

    }

    public static ArrayList<Temp> createList() throws IOException {

        ArrayList<Temp> lista = new ArrayList<Temp>();


        Double t = 0.0;
        Double h = 0.0;
        Gson gson = new Gson();
        BufferedReader bufferedReader = new BufferedReader(new FileReader("dane.txt"));
        JsonObject jsonObject = gson.fromJson(bufferedReader, JsonObject.class);
        JsonArray jsonArray = jsonObject.getAsJsonArray("list");

        for(int i=0; i < jsonArray.size(); i++){
            JsonObject object = jsonArray.get(i).getAsJsonObject();
            JsonObject object2 = object.get("main").getAsJsonObject();
            t = object2.get("temp").getAsDouble();
            h = object2.get("humidity").getAsDouble();
            lista.add(new Temp(t,h));
        }
        return lista;
    }


}

